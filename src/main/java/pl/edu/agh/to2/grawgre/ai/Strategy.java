package pl.edu.agh.to2.grawgre.ai;

import pl.edu.agh.to2.grawgre.model.GameState;
import pl.edu.agh.to2.grawgre.model.Move;
import pl.edu.agh.to2.grawgre.model.Player;

import java.lang.reflect.Array;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.lang.Math.pow;

/**
 * Created by jakubtustanowski on 10/01/2017.
 */
public abstract class Strategy {

    public Strategy(String nick)
    {
        this.nick = nick;
    }

    private String nick;

    public abstract Move makeMove(GameState gameState);

    protected Player getPlayer(List<Player> players)
    {
        for (Player player:players)
            if(player.getName() == nick)
                return player;
        return null;
    }
    protected int findMaxHash(Double[] goodRerolls, Double[] allRerolls)
    {
        double max = 0.0;
        int maxHash = 0;
        for(int i=0;i<allRerolls.length;i++)
        {
            if(((double)(goodRerolls[i])) / allRerolls[i]>max)
            {
                maxHash = i;
                max = ((double)(goodRerolls[i])) / allRerolls[i];
            }
        }
        return maxHash;
    }

    protected Move hashToMove(int hash)
    {
        Set<Integer> list= new HashSet<>();
        BigInteger big = BigInteger.valueOf(hash);
        for(int i=0;i<5;i++)
        {
            if(big.testBit(i))
                list.add(i);
        }

        return new Move(list);
    }

    protected static int nextDieValue(int current)
    {
        return current%6+1;
    }

    protected static int getRerollHash(boolean[] isRerolled) // get binary output and turn into decimal
    {
        int val = 0;
        for(int i=0;i<5;i++)
        {
            if(isRerolled[i])
            {
                val+= pow(2,i);
            }
        }
        return val;
    }

    protected int setRerollHash(int currentHash, int position, boolean value)
    {
        if(BigInteger.valueOf(currentHash).testBit(position))
        {
            if(value)
                return currentHash;
            else
                return currentHash - (int)pow(2,position);
        }
        else
        {
            if(value)
                return currentHash + (int)pow(2,position);
            else
                return currentHash;
        }
    }

    protected Move fullReroll()
    {
        return new Move(new HashSet(Arrays.asList(0, 1, 2, 3, 4)));
    }

    protected boolean isListEmpty(List<Integer> list)
    {
        for(Integer i :list)
        {
            if(i!=0)
                return false;
        }
        return true;
    }

}
