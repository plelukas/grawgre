package pl.edu.agh.to2.grawgre.server.controller;

import com.google.gson.Gson;
import org.apache.log4j.Logger;
import pl.edu.agh.to2.grawgre.server.model.ServerHuman;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import pl.edu.agh.to2.grawgre.container.*;
import pl.edu.agh.to2.grawgre.model.Game;
import pl.edu.agh.to2.grawgre.model.GameState;

import javax.jms.JMSException;
import javax.jms.TextMessage;
import java.util.List;

/**
 * Created by luke on 03.01.17.
 */

@Component
@EnableJms
public class MessageController {

    final static Logger logger = Logger.getLogger(MessageController.class.getName());

    @Autowired
    private Server server;

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private RestMessageController restMessageController;

    @Autowired
    private Gson gson;

    @JmsListener(destination = QueueNames.LOGIN_QUEUE, containerFactory = "myFactory")
    public void login(TextMessage textMessage){
        LoginContainer container;
        try {
            container = gson.fromJson(textMessage.getText(), LoginContainer.class);
            logger.info("Got LOGIN_QUEUE nick: " + container.getNick());
            Boolean isAvailable = server.checkNick(container.getNick());
            if(!isAvailable){
                sendMessage(container.getRespondQueue(), false);
            }else{
                server.addClient(container);
                sendMessage(container.getRespondQueue(), true);
            }
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    @JmsListener(destination = QueueNames.GETGAMES_QUEUE, containerFactory = "myFactory")
    public void getGames(TextMessage textMessage){
        String nick;
        try {
            nick = gson.fromJson(textMessage.getText(), String.class);
            logger.info("Got GETGAMES_QUEUE nick: " + nick);
            List<Game> games = server.getAllGames();
            sendMessage(QueueNames.GETGAMES_QUEUE + "/" + nick, games);
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    @JmsListener(destination = QueueNames.CREATEGAME_QUEUE, containerFactory = "myFactory")
    public void createGame(TextMessage textMessage){
        CreateGameContainer container;
        try {
            container = gson.fromJson(textMessage.getText(), CreateGameContainer.class);
            logger.info("Got CREATEGAME_QUEUE nick: " + container.getNick());
            Boolean result = server.handleCreateGame(container);
            sendMessage(QueueNames.CREATEGAME_QUEUE + "/" + container.getNick(), result);
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    @JmsListener(destination = QueueNames.JOINASPLAYER_QUEUE, containerFactory = "myFactory")
    public void joinAsPlayer(TextMessage textMessage){
        JoinContainer container;
        try {
            container = gson.fromJson(textMessage.getText(), JoinContainer.class);
            logger.info("Got JOINASPLAYER_QUEUE nick: " + container.getNick());
            Boolean result = server.handleJoinAsPlayer(container);
            sendMessage(QueueNames.JOINASPLAYER_QUEUE + "/" + container.getNick(), result);
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    @JmsListener(destination = QueueNames.SPECTATEGAME_QUEUE, containerFactory = "myFactory")
    public void spectateGame(TextMessage textMessage){
        JoinContainer container;
        try {
            container = gson.fromJson(textMessage.getText(), JoinContainer.class);
            logger.info("Got SPECTATEGAME_QUEUE nick: " + container.getNick());
            Boolean result = server.handleSpectateGame(container);
            sendMessage(QueueNames.SPECTATEGAME_QUEUE + "/" + container.getNick(), result);
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    @JmsListener(destination = QueueNames.MAKEMOVE_QUEUE, containerFactory = "myFactory")
    public void makeMove(TextMessage textMessage){
        MakeMoveContainer container;
        try {
            container = gson.fromJson(textMessage.getText(), MakeMoveContainer.class);
            logger.info("Got MAKEMOVE_QUEUE nick: " + container.getNick());
            Boolean result = server.handleMakeMove(container);
            sendMessage(QueueNames.MAKEMOVE_QUEUE + "/" + container.getNick(), result);
        } catch (JMSException e) {
            e.printStackTrace();
        }


    }

    @JmsListener(destination = QueueNames.QUITGAME_QUEUE, containerFactory = "myFactory")
    public void quitGame(TextMessage textMessage){
        String nick;
        try {
            nick = gson.fromJson(textMessage.getText(), String.class);
            logger.info("Got QUITGAME_QUEUE nick: " + nick);
            server.handleQuitGame(nick);
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }

    public void updateAllPeople(GameState gameState, ServerGameController serverGameController){
        logger.info("Updating all ppl from game: " + serverGameController.getGameController().getGame().getGameID());
        for(ServerHuman serverHuman: serverGameController.getServerHumen())
            if(!serverHuman.getRestClient())
                sendMessage(QueueNames.UPDATEGAME_QUEUE + "/" + serverHuman.getPlayer().getName(), gameState);
            else
                restMessageController.updatePerson(serverHuman, gameState);

        for (ServerHuman serverHuman: serverGameController.getSpectators())
            if(!serverHuman.getRestClient())
                sendMessage(QueueNames.UPDATEGAME_QUEUE + "/" + serverHuman.getPlayer().getName(), gameState);
            else
                restMessageController.updatePerson(serverHuman, gameState);
    }

    private void sendMessage(String queueName, Object message){
        jmsTemplate.convertAndSend(queueName, message);
    }

}
