package pl.edu.agh.to2.grawgre.container;

/**
 * Created by luke on 21.01.17.
 */
public class RestLoginContainer {
    private String nick;
    private String url;

    public RestLoginContainer(String nick, String url) {
        this.nick = nick;
        this.url = url;
    }

    public RestLoginContainer(){}

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
