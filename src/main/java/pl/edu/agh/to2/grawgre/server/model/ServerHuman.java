package pl.edu.agh.to2.grawgre.server.model;

import pl.edu.agh.to2.grawgre.model.GameState;
import pl.edu.agh.to2.grawgre.model.Move;

import javax.naming.TimeLimitExceededException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * Created by luke on 03.01.17.
 */
public class ServerHuman extends ServerPlayer {

    private String url;
    private Boolean isRestClient;
    private final int tickRate = 500;
    private final long timeout = 30000; // 30s

    public ServerHuman(String nick){
        super(nick);
        this.isRestClient = false;
    }

    public ServerHuman(String nick, String url){
        this(nick);
        this.url = url;
        this.isRestClient = true;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Boolean getRestClient() {
        return isRestClient;
    }

    public void setRestClient(Boolean restClient) {
        isRestClient = restClient;
    }

    public Move makeMove(GameState gameState, BlockingQueue<Object[]> taskQueue, BlockingQueue<Boolean> resultQueue)
            throws InterruptedException, TimeLimitExceededException {

        long startTime = System.currentTimeMillis();

        while(System.currentTimeMillis() - startTime < timeout){
            Object[] task = taskQueue.poll(tickRate, TimeUnit.MILLISECONDS);
            if(task != null) {
                ServerHuman human = (ServerHuman) task[0];
                Move move = (Move) task[1];

                if (human != this) {
                    System.out.println("returned false dupa");
                    // not we
                    resultQueue.put(false);
                } else {
                    resultQueue.put(true);
                    return move;
                }
            }
        }
        throw new TimeLimitExceededException("User has not responded in " + timeout/1000 + " seconds.");
    }


}
