package pl.edu.agh.to2.grawgre.server.model;

import pl.edu.agh.to2.grawgre.server.controller.ServerGameController;
import pl.edu.agh.to2.grawgre.model.GameState;
import pl.edu.agh.to2.grawgre.model.Move;
import pl.edu.agh.to2.grawgre.model.Player;

import javax.naming.TimeLimitExceededException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

/**
 * Created by luke on 03.01.17.
 */
public abstract class ServerPlayer {
    protected Player player;

    //nullable
    protected ServerGameController serverGameController;

    public ServerPlayer(String nick) {
        List<Integer> dice = new ArrayList<>(5);
        for (int i=0; i<5; i++)
            dice.add(new Integer(0));
        this.player = new Player(nick, 0, dice);
    }

    public abstract Move makeMove(GameState gameState, BlockingQueue<Object[]> taskQueue, BlockingQueue<Boolean> resultQueue)
            throws InterruptedException, TimeLimitExceededException;

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public ServerGameController getServerGameController() {
        return serverGameController;
    }

    public void setServerGameController(ServerGameController serverGameController) {
        this.serverGameController = serverGameController;
    }
}
