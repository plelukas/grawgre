package pl.edu.agh.to2.grawgre.server.model;

import pl.edu.agh.to2.grawgre.interfaces.Bot;
import pl.edu.agh.to2.grawgre.model.GameState;
import pl.edu.agh.to2.grawgre.model.Move;

import javax.naming.TimeLimitExceededException;
import java.util.concurrent.BlockingQueue;

/**
 * Created by luke on 03.01.17.
 */
public class ServerBot extends ServerPlayer {

    private Bot bot;

    public ServerBot(Bot bot) {
        super(bot.getNick());
        this.bot = bot;
    }

    public Move makeMove(GameState gameState, BlockingQueue<Object[]> taskQueue, BlockingQueue<Boolean> resultQueue)
            throws InterruptedException, TimeLimitExceededException {
        Thread.sleep(200);
        return bot.makeMove(gameState);
    }
}
