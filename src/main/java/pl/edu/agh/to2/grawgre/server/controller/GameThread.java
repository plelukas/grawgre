package pl.edu.agh.to2.grawgre.server.controller;

import com.sun.org.apache.xpath.internal.operations.Bool;
import org.apache.log4j.Logger;
import pl.edu.agh.to2.grawgre.server.exception.ElementNotFoundException;
import pl.edu.agh.to2.grawgre.server.model.ServerBot;
import pl.edu.agh.to2.grawgre.server.model.ServerHuman;
import pl.edu.agh.to2.grawgre.server.model.ServerPlayer;
import pl.edu.agh.to2.grawgre.exception.MoveNotAllowedException;
import pl.edu.agh.to2.grawgre.model.GameState;
import pl.edu.agh.to2.grawgre.model.GameStatus;
import pl.edu.agh.to2.grawgre.model.Move;
import pl.edu.agh.to2.grawgre.model.Player;

import javax.naming.TimeLimitExceededException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * Created by luke on 03.01.17.
 */
public class GameThread extends Thread {

    final static Logger logger = Logger.getLogger(GameThread.class.getName());

    private ServerGameController serverGameController;
    private MessageController messageController;
    private Server server;

    // Object[] has 2 objects: [ServerHuman, Move]
    private BlockingQueue<Object[]> taskQueue;
    private BlockingQueue<Boolean> resultQueue;

    public GameThread(ServerGameController serverGameController, MessageController messageController, Server server) {
        super();
        this.serverGameController = serverGameController;
        this.messageController = messageController;
        this.server = server;
        this.taskQueue = new ArrayBlockingQueue<>(1);
        this.resultQueue = new ArrayBlockingQueue<>(1);
    }

    public void run() {
        while(!Thread.currentThread().isInterrupted()){

            String activePlayerNick = serverGameController.getGameController().getGame().getGameState().getActivePlayer();

            // Get active ServerPlayer
            ServerPlayer activePlayer = null;
            try {
                activePlayer = getActiveServerPlayer(activePlayerNick);
            } catch (ElementNotFoundException e) {
                logger.info("ElementNotFound " + activePlayerNick);
                Player player = null;
                if(!serverGameController.getServerBots().isEmpty())
                    player = serverGameController.getServerBots().get(0).getPlayer();
                else
                    player = serverGameController.getServerHumen().get(0).getPlayer();
                GameState gameState = serverGameController.getGameController().quitGame(player);
                messageController.updateAllPeople(gameState, serverGameController);
                e.printStackTrace();
                return;
            }

            Move move = null;
            GameState gameState = null;
            try {
                // Get Move
                move = activePlayer.makeMove(serverGameController.getGameController().getGame().getGameState(),
                        taskQueue, resultQueue);

                logger.info("Got move from " + activePlayerNick);
                // Update game
                gameState = serverGameController.getGameController().makeMove(activePlayer.getPlayer(), move);
                logger.info("Correct move: " + gameState);
            } catch (InterruptedException e) {
                return;
            } catch (TimeLimitExceededException e) {
                logger.info("Timeout for player " + activePlayerNick);
                gameState = serverGameController.getGameController().timeoutMeMutherFucker();
            } catch (MoveNotAllowedException e) {
                logger.info("MoveNotAllowed " + activePlayerNick);
                if(activePlayer instanceof ServerHuman) {
                    try {
                        resultQueue.put(false);
                    } catch (InterruptedException e1) {
                        return;
                    }
                }else{
                    logger.info("Bot " + activePlayerNick + "has made illegal move!!!");
                }
            }

            logger.info("Player: " + activePlayerNick + " has made a move.");

            // Update ppl
            messageController.updateAllPeople(gameState, serverGameController);

            // Check if game has ended
            if(gameState.getStatus() == GameStatus.STOPPED){
                server.cleanUp(serverGameController);
                return;
            }
        }
    }

    private ServerPlayer getActiveServerPlayer(String nick) throws ElementNotFoundException{
        for(ServerBot bot: serverGameController.getServerBots()){
            if(bot.getPlayer().getName().equals(nick)){
                return bot;
            }
        }
        for(ServerHuman human: serverGameController.getServerHumen()){
            if(human.getPlayer().getName().equals(nick)){
                return human;
            }
        }
        throw new ElementNotFoundException("Cannot find active player in game: " +
                serverGameController.getGameController().getGame().getGameID());
    }


    public Boolean makeMove(ServerHuman serverHuman, Move move) throws InterruptedException {
        Object[] task = new Object[2];
        task[0] = serverHuman; task[1] = move;

        logger.info(serverHuman.getPlayer().getName() + " put on queue.");
        taskQueue.put(task);
        Boolean result = resultQueue.take();

        return result;
    }

}
