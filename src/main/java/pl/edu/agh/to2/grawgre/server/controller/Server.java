package pl.edu.agh.to2.grawgre.server.controller;

import pl.edu.agh.to2.grawgre.container.*;
import pl.edu.agh.to2.grawgre.server.exception.ElementNotFoundException;
import pl.edu.agh.to2.grawgre.server.model.ServerBot;
import pl.edu.agh.to2.grawgre.server.model.ServerHuman;
import org.springframework.beans.factory.annotation.Autowired;
import pl.edu.agh.to2.grawgre.exception.OperationNotAllowedException;
import pl.edu.agh.to2.grawgre.interfaces.Bot;
import pl.edu.agh.to2.grawgre.interfaces.BotFactory;
import pl.edu.agh.to2.grawgre.interfaces.GameController;
import pl.edu.agh.to2.grawgre.interfaces.GameFactory;
import pl.edu.agh.to2.grawgre.model.Game;
import pl.edu.agh.to2.grawgre.model.GameState;
import pl.edu.agh.to2.grawgre.model.GameStatus;
import pl.edu.agh.to2.grawgre.model.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by luke on 03.01.17.
 */
public class Server {

    private static Server instance;

    private GameFactory gameFactory;
    private BotFactory botFactory;

    private MessageController messageController;

    private Map<String, ServerHuman> nickHumanMap;
    private Map<Integer, ServerGameController> gameIdControllerMap;

    public Server(GameFactory gameFactory, BotFactory botFactory){
        this.gameFactory = gameFactory;
        this.botFactory = botFactory;

        nickHumanMap = new HashMap<>();
        gameIdControllerMap = new HashMap<>();
    }

    public static Server getInstance(GameFactory gameFactory, BotFactory botFactory){
        if(instance == null)
            instance = new Server(gameFactory, botFactory);
        return instance;
    }

    @Autowired
    public void setMessageController(MessageController messageController) {
        this.messageController = messageController;
    }

    public Boolean checkNick(String nick){
        return !nickHumanMap.containsKey(nick);
    }

    public void addClient(LoginContainer container){
        ServerHuman serverHuman = new ServerHuman(container.getNick());
        nickHumanMap.put(container.getNick(), serverHuman);
    }

    public void addRestClient(RestLoginContainer container){
        ServerHuman serverHuman = new ServerHuman(container.getNick(), container.getUrl());
        nickHumanMap.put(container.getNick(), serverHuman);
    }

    private ServerHuman identifyHuman(String nick){
        return nickHumanMap.get(nick);
    }

    public List<Game> getAllGames(){
        List<Game> games = new ArrayList<>(gameIdControllerMap.size());
        for(ServerGameController serverGameController: gameIdControllerMap.values()){
            games.add(serverGameController.getGameController().getGame());
        }
        return games;
    }

    public Boolean handleCreateGame(CreateGameContainer container){
        ServerHuman serverHuman = identifyHuman(container.getNick());

        if (serverHuman.getServerGameController() != null)
            return false;
        List<Bot> bots = botFactory.makeBots(container.getConfiguration());

        List<ServerBot> serverBots = makeServerBots(bots);

        List<Player> players = new ArrayList<>(serverBots.size());
        for(ServerBot serverBot: serverBots)
            players.add(serverBot.getPlayer());
        GameController gameController =
                gameFactory.createGame(container.getConfiguration(), serverHuman.getPlayer(), players);

        ServerGameController serverGameController = new ServerGameController(gameController, serverBots);
        for(ServerBot serverBot: serverBots)
            serverBot.setServerGameController(serverGameController);

//        serverGameController.getServerHumen().add(serverHuman);
//        serverHuman.setServerGameController(serverGameController);

        gameIdControllerMap.put(gameController.getGame().getGameID(), serverGameController);
        startGameIfNeeded(serverGameController);
        return true;
    }

    private List<ServerBot> makeServerBots(List<Bot> bots){
        List<ServerBot> serverBots = new ArrayList<>(bots.size());
        for(Bot bot: bots){
            serverBots.add(new ServerBot(bot));
        }
        return serverBots;
    }

    private void startGameIfNeeded(ServerGameController serverGameController){
        GameState gameState = serverGameController.getGameController().getGame().getGameState();
        if(gameState.getStatus() == GameStatus.STARTED)
            startGame(serverGameController);
    }

    public Boolean handleJoinAsPlayer(JoinContainer container){
        ServerHuman serverHuman = identifyHuman(container.getNick());

        if (serverHuman.getServerGameController() != null)
            return false;

        ServerGameController serverGameController = null;
        try {
            serverGameController = identifyServerGameController(container.getGameID());
        } catch (ElementNotFoundException e) {
            return false;
        }

        GameState gameState = null;
        try {
            gameState = serverGameController.getGameController().addPlayer(serverHuman.getPlayer());
            serverGameController.getServerHumen().add(serverHuman);
            serverHuman.setServerGameController(serverGameController);
        } catch (OperationNotAllowedException e) {
            return false;
        }

        if(gameState.getStatus() == GameStatus.STARTED)
            startGame(serverGameController);
        else
            messageController.updateAllPeople(gameState, serverGameController);

        return true;
    }

    private ServerGameController identifyServerGameController(int gameID) throws ElementNotFoundException {
        ServerGameController controller = gameIdControllerMap.get(gameID);
        if(controller == null)
            throw new ElementNotFoundException("Not found!");
        else
            return controller;
    }

    private void startGame(ServerGameController serverGameController){
        GameThread gameThread = new GameThread(serverGameController, messageController, this);
        serverGameController.setGameThread(gameThread);
        messageController.updateAllPeople(serverGameController.getGameController().getGame().getGameState(),
                serverGameController);
        gameThread.start();
    }

    public Boolean handleSpectateGame(JoinContainer container){
        ServerHuman serverHuman = identifyHuman(container.getNick());

        ServerGameController serverGameController = null;
        try {
            serverGameController = identifyServerGameController(container.getGameID());
        } catch (ElementNotFoundException e) {
            return false;
        }
        return addSpectator(serverGameController, serverHuman);
    }

    private Boolean addSpectator(ServerGameController serverGameController, ServerHuman serverHuman){
        if (serverHuman.getServerGameController() == null) {
            serverGameController.getSpectators().add(serverHuman);
            serverHuman.setServerGameController(serverGameController);
            return true;
        }
        else
            return false;
    }

    public Boolean handleMakeMove(MakeMoveContainer container){
        ServerHuman serverHuman = identifyHuman(container.getNick());

        System.out.println("dupa1");
        if (serverHuman.getServerGameController() == null || serverHuman.getServerGameController()
                .getGameController().getGame().getGameState().getStatus() != GameStatus.STARTED)
            return false;

        System.out.println("dupa2");
        Boolean result = null;
        try {
            result = serverHuman.getServerGameController().getGameThread().makeMove(serverHuman, container.getMove());
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.exit(0);
        }

        return result;
    }

    public void handleQuitGame(String nick){
        ServerHuman serverHuman = identifyHuman(nick);
        ServerGameController serverGameController = serverHuman.getServerGameController();

        if(serverGameController != null) {
            if (serverGameController.getSpectators().contains(serverHuman)) {
                serverGameController.getSpectators().remove(serverHuman);
            }
            else if (serverGameController.getServerHumen().contains(serverHuman)){
                serverGameController.getGameThread().interrupt();
                GameState gameState = serverGameController.getGameController().quitGame(serverHuman.getPlayer());
                messageController.updateAllPeople(gameState, serverGameController);
                cleanUp(serverGameController);
            }
        }
    }

    public void cleanUp(ServerGameController serverGameController) {
        gameIdControllerMap.remove(serverGameController.getGameController().getGame().getGameID());
        for(ServerHuman sv: serverGameController.getServerHumen())
            sv.setServerGameController(null);
        for(ServerHuman sv: serverGameController.getSpectators())
            sv.setServerGameController(null);
    }
}
