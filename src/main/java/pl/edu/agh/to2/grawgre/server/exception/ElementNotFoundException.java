package pl.edu.agh.to2.grawgre.server.exception;

/**
 * Created by luke on 08.01.17.
 */
public class ElementNotFoundException extends Exception {
    public ElementNotFoundException(String s) {
        super(s);
    }
}
