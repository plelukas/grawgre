package pl.edu.agh.to2.grawgre.server.controller;

import pl.edu.agh.to2.grawgre.server.model.ServerBot;
import pl.edu.agh.to2.grawgre.server.model.ServerHuman;
import pl.edu.agh.to2.grawgre.interfaces.GameController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by luke on 03.01.17.
 */
public class ServerGameController {
    //nullable
    private GameThread gameThread;
    private GameController gameController;
    private List<ServerHuman> serverHumen;
    private List<ServerBot> serverBots;
    private List<ServerHuman> spectators;

    public List<ServerHuman> getSpectators() {
        return spectators;
    }

    public void setSpectators(List<ServerHuman> spectators) {
        this.spectators = spectators;
    }

    public ServerGameController(GameController gameController, List<ServerBot> serverBots) {

        this.gameController = gameController;
        this.spectators = new ArrayList<>(0);
        this.serverBots = serverBots;
        this.serverHumen = new ArrayList<>();
    }

    public GameThread getGameThread() {
        return gameThread;
    }

    public void setGameThread(GameThread gameThread) {
        this.gameThread = gameThread;
    }

    public GameController getGameController() {
        return gameController;
    }

    public void setGameController(GameController gameController) {
        this.gameController = gameController;
    }

    public List<ServerHuman> getServerHumen() {
        return serverHumen;
    }

    public void setServerHumen(List<ServerHuman> serverHumen) {
        this.serverHumen = serverHumen;
    }

    public List<ServerBot> getServerBots() {
        return serverBots;
    }

    public void setServerBots(List<ServerBot> serverBots) {
        this.serverBots = serverBots;
    }

}
