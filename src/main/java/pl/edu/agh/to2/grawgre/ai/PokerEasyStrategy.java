package pl.edu.agh.to2.grawgre.ai;

import pl.edu.agh.to2.grawgre.model.PokerHand;

/**
 * Created by jakubtustanowski on 10/01/2017.
 */
public class PokerEasyStrategy extends PokerStrategy {
    public PokerEasyStrategy(String nick) {
        super(nick);
    }

    @Override
    protected Double assertHandValue(PokerHand tempHand, PokerHand currentBestHand) {
        return 1.0;
    }
}
