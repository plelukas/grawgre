package pl.edu.agh.to2.grawgre.ai;

import pl.edu.agh.to2.grawgre.interfaces.Bot;
import pl.edu.agh.to2.grawgre.interfaces.BotFactory;
import pl.edu.agh.to2.grawgre.model.BotConfiguration;
import pl.edu.agh.to2.grawgre.model.BotLevel;
import pl.edu.agh.to2.grawgre.model.Configuration;
import pl.edu.agh.to2.grawgre.model.GameType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jakubtustanowski on 10/01/2017.
 */
public class BotFactoryAI implements BotFactory {

    private int number;

    public BotFactoryAI()
    {
        number = 0;
    }

    @Override
    public List<Bot> makeBots(Configuration configuration) {
        List<Bot> botList = new ArrayList<>();
        for (BotConfiguration botConfiguration : configuration.getListOfBots())
        {
            botList.add(makeBot(configuration.getGameType(), botConfiguration.getBotLevel(), botConfiguration.getName()));
        }
        return botList;
    }

    public Bot makeBot(GameType gameType, BotLevel botLevel, String theirNick)
    {
        Strategy strategy = null;
        String nick = theirNick + (++number);
        switch (gameType)
        {
            case N_PLUS:
            {
                if(botLevel == BotLevel.EASY)
                    strategy = new NPlusEasyStrategy(nick);
                else
                    strategy = new NPlusHardStrategy(nick);
                break;
            }
            case N_STAR:
            {
                if(botLevel == BotLevel.EASY)
                    strategy = new NStarEasyStrategy(nick);
                else
                    strategy = new NStarHardStrategy(nick);
                break;
            }
            case POKER:
            {
                if(botLevel == BotLevel.EASY)
                    strategy = new PokerEasyStrategy(nick);
                else
                    strategy = new PokerHardStrategy(nick);
                break;
            }
        }
        return new BotAI(nick, strategy);
    }

    public int getNumber() {
        return number;
    }
}
