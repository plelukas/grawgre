package pl.edu.agh.to2.grawgre.ai;


import java.util.List;

/**
 * Created by jakubtustanowski on 10/01/2017.
 */
public class NStarEasyStrategy extends NStrategy {

    public NStarEasyStrategy(String nick) {
        super(nick);
    }


    @Override
    protected double assertHandValue(List<Integer> tempDice, int toWin, int numberOfRerolls) {
        int mul = 1;
        for(Integer die : tempDice){
            mul *= die;
        }
        if(mul==toWin) return 1.0;
        return 0;
    }
}
