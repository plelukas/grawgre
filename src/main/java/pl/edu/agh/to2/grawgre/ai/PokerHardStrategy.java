package pl.edu.agh.to2.grawgre.ai;

import pl.edu.agh.to2.grawgre.model.PokerHand;

/**
 * Created by jakubtustanowski on 10/01/2017.
 */
public class PokerHardStrategy extends PokerStrategy {

    private final int bestRankNumber = 8;

    public PokerHardStrategy(String nick) {
        super(nick);
    }

    @Override
    protected Double assertHandValue(PokerHand tempHand, PokerHand currentBestHand) {
        return 1.0 + countBonusPoints(tempHand, currentBestHand);
    }

    private int countBetterRanks(PokerHand currentBest)
    {
        return bestRankNumber - currentBest.getRank().getRank();
    }

    private int countDifference(PokerHand tempHand, PokerHand currentBestHand)
    {
        return tempHand.getRank().getRank() - currentBestHand.getRank().getRank();
    }

    private double countBonusPoints(PokerHand tempHand, PokerHand currentBestHand)
    {
        int betterRanks = countBetterRanks(currentBestHand);
        if(betterRanks == 0)
            return 0.0;
        else
            return countDifference(tempHand, currentBestHand) / betterRanks;
    }





}
